CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
 This module offering a simple Block that display time with different skin.
 You can configure clock skin from configuration page.

 Skins are as follow 
  * Digital clock with Dark skin
  * Digital clock with light skin
  * Simple Digital clock with date.
  * 24 hr Digital clock with date.

REQUIREMENTS
------------
 * This module use js library snap.svg to display analog clock.
 * To install snap.svg library visit snapsvg.io.
 * Download snap.svg-min.js and save into
 * 'drupal-root/libraries/snap.svg/snap.svg-min.js'
 * Download from
 * http://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.2.0/snap.svg-min.js

INSTALLATION
------------
 * Extract the tar.gz into your modules directory.
 * Go to "Extend" after successfully login into admin.
 * Enable the module at 'administer >> modules'.
 * Go to structure >> block and place block where you wan't to display.

CONFIGURATION
-------------
 * Go to "configuration" page.
 * Under Regional and language > Analog Digital clock.
 * Choose skin as you wish.

MAINTAINERS
-----------
 * Keshav kumar (keshav.k) - https://www.drupal.org/u/keshavk
